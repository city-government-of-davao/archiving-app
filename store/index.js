import Vue from 'vue'
import Vuex from 'vuex'

import sample from './modules/sample'
import storeevents from './modules/storeevents'

Vue.use(Vuex)

const store = () => {
  return new Vuex.Store({
    modules: {
      sample,
      storeevents
    }
  })
}

export default store
