import _ from 'lodash'

export default {
  data: () => ({
    snackbar: {
      status: false,
      color: '',
      message: '',
      interval: {},
      value: 0
    }
  }),
  methods: {
    snackbarInterval () {
      this.snackbar.interval = setInterval(() => {
        if (this.snackbar.value === 2) {
          this.snackbar.status = false
          clearInterval(this.snackbar.interval)
          return (this.snackbar.value = 0)
        }
        this.snackbar.value += 1
      }, 1000)
    },
    snackbarReturn (message, color) {
      this.snackbar.status = true
      this.snackbar.color = color
      this.snackbar.message = message
    },
    goTo (route) {
      this.$router.push(route)
    },
    isUndefined (payload) {
      if (!_.isUndefined(payload)) {
        return true
      } else {
        return false
      }
    },
    isEmpty (payload) {
      if (!_.isEmpty(payload)) {
        return true
      } else {
        return false
      }
    },
    isNull (payload) {
      if (!_.isNull(payload)) {
        return true
      } else {
        return false
      }
    },
    isObject (payload) {
      if (!_.isObject(payload)) {
        return true
      } else {
        return false
      }
    },
    returnImage (payload) {
      return _.replace(process.env.API_URL, 'api/', '') + payload
    }
  }
}
