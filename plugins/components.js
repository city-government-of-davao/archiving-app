import Vue from 'vue'

import Toolbar from '~/components/global/Toolbar'
import Navbar from '~/components/global/Navbar'
import Snackbar from '~/components/global/Snackbar'
import CustomSnackbar from '~/components/global/CustomSnackbar'

const components = {
  Toolbar,
  Navbar,
  Snackbar,
  CustomSnackbar
}

Object.entries(components).forEach(([name, component]) => {
  Vue.component(name, component)
})
